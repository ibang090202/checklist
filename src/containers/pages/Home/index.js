import React, { Component, Fragment } from 'react';
import './Home.scss'
import { addDataToAPI, getDataFromAPI, updateDataToAPI, deleteDataFromAPI } from '../../../config/redux/action/index,';
import { connect } from 'react-redux'

class Home extends Component {
    state = {
        checklistID: '',
        name: '',
        item: '',
        textButton: 'SIMPAN'
    }

    componentDidMount() {
        const token = JSON.parse(localStorage.getItem('token'))
        // console.log(token)
        console.log(this.props.isLogin)
        if(!token) {
            this.props.history.push('/login')
        } else {
            this.props.getChecklist(token)
        }
    }

    handleSaveNotes = () => {
        const { name, textButton } = this.state
        const { saveChecklist } = this.props
        const token = JSON.parse(localStorage.getItem('token'))

        const data = {
            name: name,
        }

        if (textButton === 'SIMPAN') {
            saveChecklist(data, token)
            alert("Data berhasil ditambahkan")
        } 
        
        window.location.reload()

        this.setState({
            name: '',
        })
    }

    onInputChange = (e, type) => {
        this.setState({
            [type]: e.target.value
        })
    }

    deleteChecklist = (e, list) => {
        e.stopPropagation()
        
        const token = JSON.parse(localStorage.getItem('token'))
        const data = {
            id: list.id,
            token: token
        }
        this.props.deleteNotes(data)
        
        window.location.reload()
    }

    render() {
        const { name, textButton } = this.state
        const { checklist } = this.props
        return (
            <div className="container">
                <div className="input-form">
                    <input placeholder="title checklist" className="input-title" value={name} onChange={(e) => this.onInputChange(e, 'name')} />
                    <div className="action-wrapper">
                        {
                            textButton === 'UPDATE' ? (
                                <button className="save-btn cancel " onClick={this.cancelUpdate}>CANCEL</button>
                            ) : <div />
                        }
                        <button className="save-btn" onClick={this.handleSaveNotes}>{textButton}</button>
                    </div>
                </div>
                {
                    checklist.length > 0 ? (
                        <Fragment>
                            {
                                checklist.map(list => {
                                    return (
                                        <div className="card-content" key={list.id} onClick={() => {}}>
                                            <p className="title">{list.name}</p>
                                            <div className="delete-btn" onClick={(e) => this.deleteChecklist(e, list)}>x</div>
                                        </div>
                                    )
                                })
                            }
                        </Fragment>
                    ) : (
                        <div>Loading...</div>
                    )
                }
            </div>
        );
    }
}

const reduxState = (state) => ({
    isLogin: state.isLogin,
    checklist: state.checklist
})

const reduxDispatch = (dispatch) => ({
    saveChecklist: (data, token) => dispatch(addDataToAPI(data, token)),
    updateNotes: (data) => dispatch(updateDataToAPI(data)),
    getChecklist: (data) => dispatch(getDataFromAPI(data)),
    deleteNotes: (data) => dispatch(deleteDataFromAPI(data))
})

export default connect(reduxState, reduxDispatch)(Home);