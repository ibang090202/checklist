import React, { Component } from 'react';
import { connect } from "react-redux";
import { loginUserAPI } from '../../../config/redux/action/index,';
import Button from '../../../components/atoms/Button';

class Login extends Component {
    state = {
        username: '',
        password: '',
    }
    
    handleChangeText = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleLoginSubmit = async () => {
        const { username, password } = this.state
        const { history } = this.props
        const data = {
            password: password,
            username: username
        }
        const res = await this.props.loginAPI(data).catch(err => err)
        if(res) {
            localStorage.setItem('token', JSON.stringify(res))
            this.setState({
                username: '',
                password: ''
            })
            history.push('/')
        } else {
            alert('login gagal');
        }
        
    }

    render() {
        return (
            <div className="auth-container">
                <div className="auth-card">
                    <p className="auth-title">Login Page</p>
                    <input className="input" id="username" value={this.state.username} placeholder="username" type="text" onChange={this.handleChangeText} />
                    <input className="input" id="password" value={this.state.password} placeholder="password" type="password" onChange={this.handleChangeText} />
                    <Button onClick={this.handleLoginSubmit} title="Login" loading={this.props.isLoading} />
                </div>
            </div>
        );
    }
}


const reduxState = (state) => ({
    isLoading: state.isLoading
})

const reduxDispatch = (dispatch) => ({
    loginAPI: (data) => dispatch(loginUserAPI(data))
})

export default connect(reduxState, reduxDispatch)(Login);