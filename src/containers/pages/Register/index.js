import React, { Component } from 'react';
import './Register.scss'
import Button from '../../../components/atoms/Button';
import { connect } from "react-redux";
import { registerUserAPI } from '../../../config/redux/action/index,';

class Register extends Component {
    state = {
        email: '',
        password: '',
        username: ''
    }

    handleChangeText = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleRegisterSubmit = async () => {
        const { email, password, username } = this.state
        const data = {
            email: email,
            password: password,
            username: username
        }
        const res = await this.props.registerAPI(data).catch(err => err)
        if(res) {
            this.setState({
                email: '',
                password: '',
                username: ''
            })
        }
    }

    render() {
        return (
            <div className="auth-container">
                <div className="auth-card">
                    <p className="auth-title">Register Page</p>
                    <input className="input" id="email" value={this.state.email} placeholder="email" type="text" onChange={this.handleChangeText} />
                    <input className="input" id="password" value={this.state.password} placeholder="password" type="password" onChange={this.handleChangeText} />
                    <input className="input" id="username" value={this.state.username} placeholder="username" type="username" onChange={this.handleChangeText} />
                    <Button onClick={this.handleRegisterSubmit} title="Register" loading={this.props.isLoading} />
                </div>
            </div>
        );
    }
}

const reduxState = (state) => ({
    isLoading: state.isLoading
})

const reduxDispatch = (dispatch) => ({
    registerAPI: (data) => dispatch(registerUserAPI(data))
})

export default connect(reduxState, reduxDispatch)(Register);