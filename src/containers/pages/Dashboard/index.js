import React, { Component, Fragment } from 'react';
import './Dashboard.scss'
import { addDataToAPI, getDataFromAPI, updateDataToAPI, deleteDataFromAPI } from '../../../config/redux/action/index,';
import { connect } from 'react-redux'

class Dashboard extends Component {
    state = {
        noteID: '',
        title: '',
        content: '',
        date: '',
        textButton: 'SIMPAN'
    }

    componentDidMount() {
        const dataUser = JSON.parse(localStorage.getItem('dataUser'))

        if(!this.props.isLogin) {
            this.props.history.push('/login')
        } else {
            this.props.getNotes(dataUser.uid)
        }
    }

    handleSaveNotes = () => {
        const { noteID, title, content, textButton } = this.state
        const { saveNotes, updateNotes } = this.props
        const dataUser = JSON.parse(localStorage.getItem('dataUser'))

        const data = {
            title: title,
            content: content,
            date: new Date().getTime(),
            userID: dataUser.uid
        }
        // console.log(data)

        if (textButton === 'SIMPAN') {
            saveNotes(data)
            alert("Data berhasil ditambahkan")
        } else {
            data.noteID = noteID
            updateNotes(data)
        }

        this.setState({
            title: '',
            content: ''
        })
    }

    onInputChange = (e, type) => {
        this.setState({
            [type]: e.target.value
        })
    }

    updateNotes = (note) => {
        this.setState({
            noteID: note.id,
            title: note.data.title,
            content: note.data.content,
            textButton: 'UPDATE'
        })
    }

    cancelUpdate = () => {
        this.setState({
            noteID: '',
            title: '',
            content: '',
            textButton: 'SIMPAN'
        })
    }

    deleteNote = (e, note) => {
        e.stopPropagation()
        
        const dataUser = JSON.parse(localStorage.getItem('dataUser'))
        const data = {
            userID: dataUser.uid,
            noteID: note.id
        }
        this.props.deleteNotes(data)
    }

    render() {
        const { title, content, textButton } = this.state
        const { notes } = this.props
        // console.log('notes: ', notes)
        return (
            <div className="container">
                <div className="input-form">
                    <input placeholder="title" className="input-title" value={title} onChange={(e) => this.onInputChange(e, 'title')} />
                    <textarea placeholder="content" className="input-content" value={content} onChange={(e) => this.onInputChange(e, 'content')} ></textarea>
                    <div className="action-wrapper">
                        {
                            textButton === 'UPDATE' ? (
                                <button className="save-btn cancel " onClick={this.cancelUpdate}>Cancel</button>
                            ) : <div />
                        }
                        <button className="save-btn" onClick={this.handleSaveNotes}>{textButton}</button>
                    </div>
                </div>
                <hr />
                {
                    notes.length > 0 ? (
                        <Fragment>
                            {
                                notes.map(note => {
                                    return (
                                        <div className="card-content" key={note.id} onClick={() => this.updateNotes(note)}>
                                            <p className="title">{note.data.title}</p>
                                            <p className="date">{note.data.date}</p>
                                            <p className="content">{note.data.content}</p>
                                            <div className="delete-btn" onClick={(e) => this.deleteNote(e, note)}>x</div>
                                        </div>
                                    )
                                })
                            }
                        </Fragment>
                    ) : (
                        <div>haii</div>
                    )
                }
                
            </div>
        );
    }
}

const reduxState = (state) => ({
    isLogin: state.isLogin,
    userData: state.user,
    notes: state.notes
})

const reduxDispatch = (dispatch) => ({
    saveNotes: (data) => dispatch(addDataToAPI(data)),
    updateNotes: (data) => dispatch(updateDataToAPI(data)),
    getNotes: (data) => dispatch(getDataFromAPI(data)),
    deleteNotes: (data) => dispatch(deleteDataFromAPI(data))
})

export default connect(reduxState, reduxDispatch)(Dashboard);