const initState = {
    popup: false,
    isLogin: false,
    isLoading: false,
    user: {},
    notes: [],
    checklist: []
}

const reducer = (state = initState, action) => {
    switch(action.type) {
        case 'CHANGE_POPUP':
            return { ...state, popup: action.value }
        case 'CHANGE_ISLOGIN':
            return { ...state, isLogin: action.value }
        case 'CHANGE_USER':
            return { ...state, user: action.value }
        case 'CHANGE_ISLOADING':
            return { ...state, isLoading: action.value }
        case 'SET_NOTES':
            return { ...state, notes: action.value }
        case 'SET_CHECKLIST':
            return { ...state, checklist: action.value }
        default:
            return state
    }
}

export default reducer