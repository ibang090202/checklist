import firebase, { database } from '../../firebase'
import axios from 'axios'
export const actionUserName = () => (dispatch) => {
    setTimeout(() => {
        return dispatch({type: 'CHANGE_USER', value: 'M IQBAL ALAMSYAH S'})
    }, 2000)
}

export const registerUserAPI = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        dispatch({type: 'CHANGE_ISLOADING', value: true})
        return (
            axios.post('http://18.141.178.15:8080/register',data)
            .then((res) => {
                console.log('success', res)
                dispatch({type: 'CHANGE_ISLOADING', value: false})
                resolve(true)
            })
            .catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorCode, errorMessage);
                dispatch({type: 'CHANGE_ISLOADING', value: false})
                reject(false)
            })
        )
    })
}

export const loginUserAPI = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        dispatch({type: 'CHANGE_ISLOADING', value: true})
        return (
            axios.post('http://18.141.178.15:8080/login',data)
            .then((res) => {
                // console.log('success', res.data.data.token)
                // const dataUser = {
                //     // email : res.user.email,
                //     // uid: res.user.uid,
                //     // emailVerified: res.user.emailVerified,
                //     // refreshToken: res.user.refreshToken
                // }
                dispatch({type: 'CHANGE_ISLOADING', value: false})
                dispatch({type: 'CHANGE_ISLOGIN', value: true})
                resolve(res.data.data.token)
            })
            .catch(function(error) {
                // Handle Errors here.
                // var errorCode = error.code;
                // var errorMessage = error.message;
                // console.log(errorCode, errorMessage);
                dispatch({type: 'CHANGE_ISLOADING', value: false})
                dispatch({type: 'CHANGE_ISLOGIN', value: false})
                reject(false)
            })
        )
        
    })
}

export const addDataToAPI = (data, token) => (dispatch) => {
    return new Promise((resolve, reject) => {
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        axios.post('http://18.141.178.15:8080/checklist', data, config)
        .then(res => {
            console.log('ini hasil simpan checklist : ',res)
            resolve(res)
        })
        .catch(console.log);
    })
}

export const getDataFromAPI = (token) => (dispatch) => {
    // const url = database.ref('notes/'+userID);
    return new Promise((resolve, reject) => {
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        
        axios.get('http://18.141.178.15:8080/checklist',config)
        .then(res => {
            dispatch({type: 'SET_CHECKLIST', value: res.data.data})
            resolve(res)
        })
        .catch(console.log);
    })
}

export const updateDataToAPI = (data) => (dispatch) => {
    const url = database.ref(`notes/${data.userID}/${data.noteID}`);
    return new Promise((resolve, reject) => {
        url.set({
            title: data.title,
            content: data.content,
            date: data.date
        }, (err) => {
            if (err) {
                reject(false)
            } else {
                resolve(true)
            }
        })
    })
}

export const deleteDataFromAPI = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        const config = {
            headers: { Authorization: `Bearer ${data.token}` }
        };

        axios.delete(`http://18.141.178.15:8080/checklist/${data.id}`, config)
        // .then(res => {
        //     console.log('ini hasil delete checklist : ',res)
        //     resolve(res)
        // })
        // .catch(console.log);
    })
}

// import firebase, { database } from '../../firebase'

// export const actionUserName = () => (dispatch) => {
//     setTimeout(() => {
//         return dispatch({type: 'CHANGE_USER', value: 'M IQBAL ALAMSYAH S'})
//     }, 2000)
// }

// export const registerUserAPI = (data) => (dispatch) => {
//     return new Promise((resolve, reject) => {
//         dispatch({type: 'CHANGE_ISLOADING', value: true})
//         return (
//             firebase.auth().createUserWithEmailAndPassword(data.email, data.password)
//             .then((res) => {
//                 console.log('success', res)
//                 dispatch({type: 'CHANGE_ISLOADING', value: false})
//                 resolve(true)
//             })
//             .catch(function(error) {
//                 // Handle Errors here.
//                 var errorCode = error.code;
//                 var errorMessage = error.message;
//                 console.log(errorCode, errorMessage);
//                 dispatch({type: 'CHANGE_ISLOADING', value: false})
//                 reject(false)
//             })
//         )
//     })
// }

// export const loginUserAPI = (data) => (dispatch) => {
//     return new Promise((resolve, reject) => {
//         dispatch({type: 'CHANGE_ISLOADING', value: true})
//         return (
//             firebase.auth().signInWithEmailAndPassword(data.email, data.password)
//             .then((res) => {
//                 // console.log('success', res)
//                 const dataUser = {
//                     email : res.user.email,
//                     uid: res.user.uid,
//                     emailVerified: res.user.emailVerified,
//                     refreshToken: res.user.refreshToken
//                 }
//                 dispatch({type: 'CHANGE_ISLOADING', value: false})
//                 dispatch({type: 'CHANGE_ISLOGIN', value: true})
//                 dispatch({type: 'CHANGE_USER', value: dataUser})
//                 resolve(dataUser)
//             })
//             .catch(function(error) {
//                 // Handle Errors here.
//                 // var errorCode = error.code;
//                 // var errorMessage = error.message;
//                 // console.log(errorCode, errorMessage);
//                 dispatch({type: 'CHANGE_ISLOADING', value: false})
//                 dispatch({type: 'CHANGE_ISLOGIN', value: false})
//                 reject(false)
//             })
//         )
        
//     })
// }

// export const addDataToAPI = (data) => (dispatch) => {
//     database.ref('notes/'+data.userID).push({
//         title: data.title,
//         content: data.content,
//         date: data.date
//     })
// }

// export const getDataFromAPI = (userID) => (dispatch) => {
//     const url = database.ref('notes/'+userID);
//     return new Promise((resolve, reject) => {
//         url.on('value', function(snapshot) {
//             console.log('Get Data: ', snapshot.val())
//             const data = []
//             if (snapshot.val() != null) {
//                 Object.keys(snapshot.val()).map(key => {
//                     data.push({
//                         id: key,
//                         data: snapshot.val()[key]
//                     })
//                 })
//             }
//             dispatch({type: 'SET_NOTES', value: data})
//             resolve(snapshot.val())
//         })
//     })
// }

// export const updateDataToAPI = (data) => (dispatch) => {
//     const url = database.ref(`notes/${data.userID}/${data.noteID}`);
//     return new Promise((resolve, reject) => {
//         url.set({
//             title: data.title,
//             content: data.content,
//             date: data.date
//         }, (err) => {
//             if (err) {
//                 reject(false)
//             } else {
//                 resolve(true)
//             }
//         })
//     })
// }

// export const deleteDataFromAPI = (data) => (dispatch) => {
//     const url = database.ref(`notes/${data.userID}/${data.noteID}`);
//     return new Promise((resolve, reject) => {
//         url.remove()
//     })
// }