import firebase from "firebase/app";
import 'firebase/auth' 
import 'firebase/database'

const firebaseConfig = {
    apiKey: "AIzaSyCN0F5jPiN2VT_R_TWPc6NxVVQvhYyY0B4",
    authDomain: "note-bang.firebaseapp.com",
    databaseURL: "https://note-bang.firebaseio.com",
    projectId: "note-bang",
    storageBucket: "note-bang.appspot.com",
    messagingSenderId: "147163152900",
    appId: "1:147163152900:web:39afb71af88e4ba2caab51",
    measurementId: "G-5FCGVE3E08"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const database = firebase.database();
// firebase.analytics();

export default firebase